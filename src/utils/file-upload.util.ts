import { BadRequestException } from '@nestjs/common';

export const fileFilter = (req, file, callback) => {
  if (
    file.mimetype !==
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
  ) {
    return callback(new BadRequestException('FILE_TYPE_ERR'), false);
  }
  callback(null, true);
};
