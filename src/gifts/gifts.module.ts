import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  GiftRepository,
  OrderRepository,
  UserRepository,
} from '@app/gifts/repositories';
import { GiftsController } from '@app/gifts/gifts.controller';
import { GiftsService } from '@app/gifts/gifts.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserRepository, GiftRepository, OrderRepository]),
  ],
  controllers: [GiftsController],
  providers: [GiftsService],
})
export class GiftsModule {}
