import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { IOrder } from '@app/gifts/interfaces';

@Entity('order')
export class OrderEntity implements IOrder {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  date: Date;

  @Column({ nullable: true })
  description: string;

  @Column({ nullable: false })
  userId: number;

  @Column({ nullable: false })
  giftId: number;
}
