import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { IUser } from '@app/gifts/interfaces';

@Entity('user')
export class UserEntity implements IUser {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  name: string;

  @Column({ nullable: false })
  number: string;
}
