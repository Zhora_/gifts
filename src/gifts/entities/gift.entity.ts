import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { IGift } from '@app/gifts/interfaces';

@Entity('gift')
export class GiftEntity implements IGift {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  name: string;

  @Column({ nullable: false })
  fromId: number;
}
