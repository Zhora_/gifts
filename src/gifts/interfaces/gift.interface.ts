export interface IGift {
  id?: number;
  name: string;
  fromId: number;
}
