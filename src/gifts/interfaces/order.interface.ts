export interface IOrder {
  id?: number;
  date: Date;
  description: string;
  userId: number;
  giftId: number;
}
