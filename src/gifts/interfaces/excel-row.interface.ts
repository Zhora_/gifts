export interface IExcelRow {
  id: number;
  order_create_date: string;
  user: string;
  gift: string;
  gift_from_label: string;
  order_description: string;
}
