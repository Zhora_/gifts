export interface IUser {
  id?: number;
  name: string;
  number: string;
}
