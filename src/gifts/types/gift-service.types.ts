import { HashMap } from '@app/gifts/types/utlity.types';
import { IExcelRow, IUser } from '@app/gifts/interfaces';

export type StoreExcelDataActionPayload = {
  file: Express.Multer.File;
};

export type StoreExcelDataActionResult = {
  result: boolean;
};

export type GetExcelDataPayload = {
  file: Express.Multer.File;
};

export type TransformWorksheetPayload = {
  worksheet: any;
  columnsHashMap: HashMap<string>;
};

export type TransformWorksheetResult = IExcelRow[];

export type GetColumnNamesPayload = {
  worksheet: any;
  nameIndex: number;
};
export type GetColumnNamesResult = HashMap<string>;

export type GetUserEntitiesPayload = {
  rows: IExcelRow[];
};

export type GetUserEntitiesResult = Array<{ name: string; number: string }>;

export type GetUsersHashMapPayload = {
  users: IUser[];
};

export type GetUsersHashMapResult = HashMap<number>;
