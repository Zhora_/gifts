export type HashMap<V> = { [key: string | number]: V };
