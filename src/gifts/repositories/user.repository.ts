import { EntityRepository, Repository } from 'typeorm';
import { UserEntity } from '@app/gifts/entities/user.entity';

@EntityRepository(UserEntity)
export class UserRepository extends Repository<UserEntity> {}
