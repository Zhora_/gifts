import { EntityRepository, Repository } from 'typeorm';
import { OrderEntity } from '@app/gifts/entities/order.entity';

@EntityRepository(OrderEntity)
export class OrderRepository extends Repository<OrderEntity> {}
