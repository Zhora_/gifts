import { Injectable } from '@nestjs/common';
import * as ExcelJS from 'exceljs';
import { InjectRepository } from '@nestjs/typeorm';

import {
  UtilityTypes,
  GiftServiceTypes as GiftService,
} from '@app/gifts/types';
import {
  GiftRepository,
  OrderRepository,
  UserRepository,
} from '@app/gifts/repositories';
import HashMap = UtilityTypes.HashMap;
import { IExcelRow, IGift, IOrder, IUser } from '@app/gifts/interfaces';

@Injectable()
export class GiftsService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    @InjectRepository(GiftRepository)
    private giftRepository: GiftRepository,
    @InjectRepository(OrderRepository)
    private orderRepository: OrderRepository,
  ) {}

  async storeExcelDataAction({
    file,
  }: GiftService.StoreExcelDataActionPayload): Promise<GiftService.StoreExcelDataActionResult> {
    const rows: IExcelRow[] = await this.getExcelData({ file });
    const users: IUser[] = this.getUserEntities({ rows });

    await this.userRepository.insert(users);

    const usersHashMap: HashMap<number> = this.getUsersHashMap({
      users,
    });

    for (const row of rows) {
      const giftRow: IGift = {
        name: row.gift,
        fromId: usersHashMap[row.gift_from_label],
      };

      await this.giftRepository.insert(giftRow);

      const [date, time] = row.order_create_date.split(' ');

      const orderRow: IOrder = {
        date: new Date(`${date} ${time}`),
        description: row.order_description || null,
        userId: usersHashMap[row.user],
        giftId: giftRow.id,
      };

      await this.orderRepository.insert(orderRow);
    }

    return {
      result: true,
    };
  }

  async getExcelData({
    file,
  }: GiftService.GetExcelDataPayload): Promise<GiftService.TransformWorksheetResult> {
    const workbook: ExcelJS.Workbook = new ExcelJS.Workbook();
    const data: any = await workbook.xlsx.load(file.buffer);
    const worksheet: ExcelJS.Worksheet = data.getWorksheet('Sheet1');
    const columnsHashMap: HashMap<string> = this.getColumnNames({
      worksheet,
      nameIndex: 2,
    });

    return this.transformWorksheet({ worksheet, columnsHashMap });
  }

  getUserEntities({
    rows,
  }: GiftService.GetUserEntitiesPayload): GiftService.GetUserEntitiesResult {
    const users: string[][] = rows.map((item) => {
      return [item.user, item.gift_from_label];
    });

    const uniqueUsers: string[] = [...new Set(users.flat())];

    return uniqueUsers.map((userRow) => {
      const [number, name]: string[] = userRow.split(' ');
      return { name, number };
    });
  }

  getColumnNames({
    worksheet,
    nameIndex,
  }: GiftService.GetColumnNamesPayload): GiftService.GetColumnNamesResult {
    const keyRow: ExcelJS.Row = worksheet.getRow(nameIndex);
    const keyObj: HashMap<string> = {};

    for (let k = 1; k <= worksheet.columnCount; k++) {
      const { col, value } = keyRow.getCell(k);
      keyObj[col] = value.toString().replace(/ /g, '_');
    }

    return keyObj;
  }

  getUsersHashMap({
    users,
  }: GiftService.GetUsersHashMapPayload): GiftService.GetUsersHashMapResult {
    return users.reduce((acc, user) => {
      acc[`${user.number} ${user.name}`] = user.id;
      return acc;
    }, {});
  }

  transformWorksheet({
    worksheet,
    columnsHashMap,
  }: GiftService.TransformWorksheetPayload): GiftService.TransformWorksheetResult {
    const items = [];

    for (let i = 3; i <= worksheet.rowCount; i++) {
      const item: Record<string, any> = {};
      for (let j = 1; j <= worksheet.columnCount; j++) {
        const { col, value } = worksheet.getRow(i).getCell(j);

        item[columnsHashMap[col]] = value;
      }

      items.push(item);
    }

    return items;
  }
}
