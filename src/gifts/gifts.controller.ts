import {
  BadRequestException,
  Controller,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { GiftsService } from '@app/gifts/gifts.service';
import { fileFilter } from '@app/utils/file-upload.util';

@ApiTags('Gifts')
@Controller('gifts')
export class GiftsController {
  constructor(private giftsService: GiftsService) {}

  @Post('')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(FileInterceptor('file', { fileFilter }))
  async uploadFile(@UploadedFile() file: any): Promise<{ result: boolean }> {
    if (!file) {
      throw new BadRequestException('ERR_FILE_IS_REQUIRED');
    }

    return this.giftsService.storeExcelDataAction({ file });
  }
}
