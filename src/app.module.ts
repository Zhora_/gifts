import { Module } from '@nestjs/common';

import { ConfigModule } from '@nestjs/config';
import { GiftsModule } from '@app/gifts/gifts.module';
import { DatabaseModule } from '@app/database/database.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    GiftsModule,
    DatabaseModule,
  ],
})
export class AppModule {}
