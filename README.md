<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>


## Running the app

```bash
$ docker-compose up --build -d
```

## Apidoc

```bash
$ localhost:3000/apidoc/
```